<?php get_template_part('includes/header'); ?>

<button class="hamburger hamburger--emphatic">
  <span class="hamburger-box">
    <span class="hamburger-inner"></span>
  </span>
</button>
<nav class='container bk-primary-nav'>
  <ul id="menu" class="bk-primary-nav__menu">
      <li data-menuanchor="HOP" class="active bk-primary-nav__menu-item"><a href="#beHop" class="p-2">HOP</a></li>
      <li data-menuanchor="HOP SCOOTER" class="bk-primary-nav__menu-item"><a href="#timeFun" class="p-2">HOP SCOOTER</a></li>
      <li data-menuanchor="HOW TO HOP" class="bk-primary-nav__menu-item"><a href="#detail" class="p-2">HOW TO HOP</a></li>
      <li data-menuanchor="APP" class="bk-primary-nav__menu-item"><a href="#howTo" class="p-2">APP</a></li>
      <li data-menuanchor="HOPPERS" class="bk-primary-nav__menu-item"><a href="#social" class="p-2">HOPPERS</a></li>
      <li data-menuanchor="RIDE SAFE" class="bk-primary-nav__menu-item"><a href="#ride" class="p-2">RIDE SAFE</a></li>
      <li data-menuanchor="HOP CITY" class="bk-primary-nav__menu-item"><a href="#city" class="p-2">HOP CITY</a></li>
  </ul>
</nav>
<div class="bk-vertical-logo dn">
  <img src="<?php bloginfo('template_directory'); ?>/assets/img/vertical-logo.png" alt="">
</div>
<main id="fullpage">

  <section class="section bg-01 bk-sec-01">
 
<!--     <video id="hopVideo" loop data-autoplay>
			<source data-src="<?php bloginfo('template_directory'); ?>/assets/img/hopsl.mp4" type="video/mp4">
		</video>  -->
    
    <div class="container bk-sec-01--content">
      <div class="row align-items-center">
        <div class="col-md-8">
          <h2 class="bk-title--primary pt-4 pb-4">PLATAFORMA <br>DE TRANSPORTE ELÉCTRICO COMPARTIDO</h2>
          <div class="bk-be-ahopper text-white"><h5><span class="bk-be-ahopper">Be a Hopper</span><b> #daelsalto y pide tu propia flota</b></h5></div>
          <div class="pt-4 text-white cd-headline loading-bar">
            <div class="bk-text-wrapper cd-words-wrapper">
              <p class="bk-hop-txt is-visible bk-sec-01__txt">Transporte colaborativo que llega para devolverle el tiempo a las personas, disminuir la contaminación y descongestionar las calles de la ciudad.</p>
              <p class="bk-hop-txt is-hidden bk-sec-01__txt">Un movimiento nuevo, que revolucionará tu forma de vivir y moverte a través de HOP´S eléctricos.</p>
              </div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="text-right d-none d-md-block">
            <img class="bk-sec-01__logo" src="<?php bloginfo('template_directory'); ?>/assets/img/hop-logo-3.png" alt="">
          </div>
        </div>
      </div>
    </div>
    <?php echo do_shortcode('[rev_slider alias="hop_2"]');?>
  </section>

  <section class="section bg-02 bk-sec-02">
    <div class="container">
      <div class="row justify-content-end">
        <div class="col-md-5 d-md-none pb-5">
          <h2 class="bk-title--primary"><span>#DAELSALTO</span><br>Y TEN TU PROPIA FLOTA.</h2>
        </div>
        <div class="col-md-5 text-white">
          <h4 class="text-white pb-2 bk-sec-02__title bk-title--secundary">TU FLOTA ¡EL NEGOCIO ES TUYO!</h4>
          <p class="">Se parte de nuestra comunidad y juntos llevemos transporte compartido a todas las regiones, ciudades y países del mundo en búsqueda de una mejor calidad de vida. </p>
          <!-- <p class="text-right">No necesitas estar en Silicon Valley para emprender en Tecnología y Movilidad. Se parte de la nueva moda de transporte compartido sin tener que manejar 1000 variables!.</p> -->
          <div class="bk-sec-02__card">
            <div class="text-white cd-headline loading-bar">
              <div class="bk-text-wrapper cd-words-wrapper">
                <h4 class="bk-hop-txt is-visible bk-sec-02__title bk-title--secundary"><span class=" bk-hop-number">1</span> Seamos socios. Buscamos compartir el negocio</h4>
                <h4 class="bk-hop-txt is-hidden bk-sec-02__title bk-title--secundary"><span class=" bk-hop-number">2</span> Te damos la plataforma, tu operas y te llevas la mayor parte de los beneficios.</h4>
                <h4 class="bk-hop-txt is-hidden bk-sec-02__title bk-title--secundary"><span class=" bk-hop-number">3</span>  Te hacemos la vida fácil. Solo te preocupas de tu flota, la carga y mantención. (tenemos alianzas)</h4>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-5">
          <h2 class="bk-title--primary d-none d-md-block"><span>#DAELSALTO</span><br>Y TEN TU PROPIA FLOTA.</h2>
        </div>
      </div>
      <div class="row"></div>
    </div>
  </section>
  
  <section class="section bg-03 bk-sec-03">
    <div class="container">
      <div class="row align-items-center">

        <div class="col-lg-6">
          <h2 class="bk-title--primary"><span>NUEVO DISEÑO,</span> CONSTRUIDO PARA COMPARTIR.</h2>
        </div>

        <div class="col-lg-6">

          <img class="mapa" src="<?php bloginfo('template_directory'); ?>/assets/img/ride.png" usemap="#image-map" alt="First slide">

          <map name="image-map">
              <area data-key="TX" class="dk-TX" target="" alt="Frenos con tecnología E-ABS" title="" href="" coords="283,187,30" shape="circle">
              <area data-key="BT" class="dk-BT" target="" alt="25 km/h, 2km en 6 minutos" title="" href="" coords="442,188,30" shape="circle">
              <area data-key="SD" class="dk-SD" target="" alt="Bateria HOP: Autonomía 55km" title="" href="" coords="420,359,30" shape="circle">
              <area data-key="FR" class="dk-FR" target="" alt="Fibra de carbono inoxidable" title="" href="" coords="283,517,27" shape="circle">
              <area data-key="MT" class="dk-MT" target="" alt="Capacidad de carga: 120kg" title="" href="" coords="513,555,26" shape="circle">
          </map>

          <div class="bk-map--tooltip"></div>
        </div>

      </div>
    </div>
  </section>

  <section class="section bg-04 bk-sec-04">
    <div class="container">
      <div class="row align-items-center">

        <div class="col-md-6">
 
          <div id="slider" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner bk-phone--carousel">

              <div class="carousel-item active">

                  <img class="w-100" src="<?php bloginfo('template_directory'); ?>/assets/img/screen-1.png" alt="First slide">

                  <!-- <img class="w-100" src="<?php bloginfo('template_directory'); ?>/assets/img/how-app-01.png" alt="First slide"> -->

              </div>

              <div class="carousel-item">

                  <img class="w-100" src="<?php bloginfo('template_directory'); ?>/assets/img/screen-2.png" alt="First slide">

                  <!-- <img class="w-100" src="<?php bloginfo('template_directory'); ?>/assets/img/how-app-02.png" alt="First slide"> -->

              </div>

              <div class="carousel-item">

                  <img class="w-100" src="<?php bloginfo('template_directory'); ?>/assets/img/screen-3.png" alt="First slide">

                  <!-- <img class="w-100" src="<?php bloginfo('template_directory'); ?>/assets/img/how-app-03.png" alt="First slide">  -->

              </div>

              <div class="carousel-item">

                <img class="w-100" src="<?php bloginfo('template_directory'); ?>/assets/img/screen-4.png" alt="First slide">

              <!-- <img class="w-100" src="<?php bloginfo('template_directory'); ?>/assets/img/how-app-03.png" alt="First slide">  -->

              </div>

              <div class="carousel-item">

                <img class="w-100" src="<?php bloginfo('template_directory'); ?>/assets/img/screen-5.png" alt="First slide">

                <!-- <img class="w-100" src="<?php bloginfo('template_directory'); ?>/assets/img/how-app-03.png" alt="First slide">  -->

              </div>

            </div>
            <a class="carousel-control-prev" href="#slider" role="button" data-slide="prev">
              <span class="carousel-control-prev-icon" aria-hidden="true"></span>
              <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#slider" role="button" data-slide="next">
              <span class="carousel-control-next-icon" aria-hidden="true"></span>
              <span class="sr-only">Next</span>
            </a>
          </div>

        </div>

        <div class="col-md-6 pt-4">
          <h2 class="bk-title--primary"><span>CÓMO</span> USAR</h2>
          <div class="bk-sec-04__logo">
            <img class="dib" src="<?php bloginfo('template_directory'); ?>/assets/img/span-img.png" alt="First slide">
          </div>

          <div id="slider_captions" class="slider_captions" >
              <div id="caption-0" class="bk-carousel-caption caption-1 text-white pt-5 bk-sec-04__txt" data-id="0">
                <p>Descarga la aplicaciòn</p>
                <p><small>Disponible en AppStore y Google Play</small></p>
              </div>
              <div id="caption-1" class="bk-carousel-caption caption-1 text-white pt-5 bk-sec-04__txt" data-id="1">
                <p>Registrate o ingresa</p>
                <p><small>Ingresa tus datos en la aplicación</small></p>
              </div>
              <div id="caption-2" class="bk-carousel-caption caption-2 text-white pt-5 bk-sec-04__txt" data-id="2">
                <p>Busca el scooter más cercano</p>
                <p><small>Utiliza la aplicación para encontrar dispositivos disponibles</small></p>
              </div>
              <div id="caption-3" class="bk-carousel-caption caption-3 text-white pt-5 bk-sec-04__txt" data-id="3">
                <p>Escanea el código QR</p>
                <p><small>Utiliza la camara de tu móvil</small></p>
            </div>
            <div id="caption-4" class="bk-carousel-caption caption-3 text-white pt-5 bk-sec-04__txt" data-id="4">
                <p>Recorre tu ciudad ¡Y sobre todo disfruta!</p>
                <p><small>¡Recuerda! Siempre finalizar tu viaje...</small></p>
            </div>

          </div>

        </div>

      </div>
    </div>
  </section>

  <section class="section bg-04-00 bk-sec-04-00">
    <div class="container">
      <div class="row align-items-center">
        <div class="col-md">
          <div class="d-none d-md-block">
            <img class="bk-sec-01__logo" src="<?php bloginfo('template_directory'); ?>/assets/img/bk-hero-logo.png" alt="">
          </div>
        </div>
        <div class="col-md">
          <h2 class="bk-title--primary"><span>#DAELSALTO</span><br>Y TEN TU PROPIA FLOTA</h2>
          <p class="text-white">Somos una comunidad de HOPPERS cansados de los tacos, contaminación y estrés de las ciudades. Creemos firmemente en que “compartir nos ayuda a vivir”, es por eso que queremos llevar transporte compartido a todas las regiones, países y culturas. Se parte de nuestra misión.</p>
          <ul class="text-white">
            <li>
              <p><span class="bk-sec-04-00__span">Devolvámonos</span> el tiempo a nosotros mismos.</p>
            </li>
            <li>
              <p><span class="bk-sec-04-00__span">Disminuyamos</span> la huella de carbono.</p>
            </li>
            <li>
              <p><span class="bk-sec-04-00__span">Descongestionemos</span> nuestras ciudades.</p>
            </li>
          </ul>
        </div>
      </div>
    </div>
  </section>

  <section class="section bg-04-01 bk-sec-04-01">
    <div class="container">
      <div class="row align-items-center">
      <div class="col-md-6">
          <h2 class="bk-title--grey pb-2">SAY HELLO!</h2>
        </div>
        <div class="col-md-6">
          <div class="bk-sec-06__card">
            <h3 class="bk-grey">RECIBE MÁS INFORMACIÓN SOBRE CÓMO CONVERTIRTE EN SOCIO</h3>
            <?php echo do_shortcode('[contact-form-7 id="12" title="Suscribir"]'); ?>
          </div>
        </div>
      </div>
    </div>
  </section>
  <section class="section bg-05 bk-sec-05">
    <div class="grid">

    <?php $data_args = array(
        'post_type'      => 'hop',
        'posts_per_page' => 6
    );
    $data_loop = new WP_Query( $data_args ); 
    ?>
    <?php if ( $data_loop->have_posts() ) :?>
    <?php while ($data_loop->have_posts()) : $data_loop->the_post();?>
    
    <?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );?>
    <div class="grid-item" style="background-image: url('<?php echo $thumb['0'];?>')">
      <a href="<?php  echo $thumb['0']; ?>" data-toggle="lightbox" data-gallery="hop-gallery" data-title="Cerrar" data-footer="<?php the_excerpt()?>">
        <div class="bk-sec-05--01__wrapper"></div>
      </a>
      <div class="bk-sec-05--01__txt text-white">
        <p><?php the_title()?></p>
        <small><?php echo get_the_date('M  d , y'); ?></small>
      </div>
    </div>
    <?php endwhile; wp_reset_postdata();?>
    <?php endif; wp_reset_postdata();?>

  </section>

  <section class="pt-4 section bg-06 bk-sec-06">
    <div class="container">
    <div class="row">
        <div class="col-md-6">
          <h2 class="bk-title--grey pb-2">RIDE SAFE</h2>
        </div>
        <div class="col-md-6">
          <div class="bk-sec-06__card">
            <h3 class="bk-grey">MUÉVETE A TU RITMO</h3>
            <p>Por tu propia seguridad este servicio es sólo para mayores de 18 años con Licencia de Conducir vigente.</p>
          </div>
        </div>
    </div>
    <div class="row mt-5 bk-sec-06__line">
      <div class="col-sm-3">
        <div class="bk-sec-06__img">
          <img src="<?php bloginfo('template_directory'); ?>/assets/img/helmet.png" onmouseover="this.src='<?php bloginfo('template_directory'); ?>/assets/img/helmet-hover.png'" onmouseout="this.src='<?php bloginfo('template_directory'); ?>/assets/img/helmet.png'" alt="HOP" style="width:80%; margin:0 auto;">
          <div class="bk-sec-06__txt"><span>Casco</span></div>
        </div>
      </div>
      <div class="col-sm-3">
        <div class="bk-sec-06__img">
          <img src="<?php bloginfo('template_directory'); ?>/assets/img/move.png" onmouseover="this.src='<?php bloginfo('template_directory'); ?>/assets/img/move-hover.png'" onmouseout="this.src='<?php bloginfo('template_directory'); ?>/assets/img/move.png'" alt="HOP" style="width:80%; margin:0 auto;">
          <div class="bk-sec-06__txt"><span>Muévete</span></div>
        </div>
      </div>
      <div class="col-sm-3">
        <div class="bk-sec-06__img">
          <img src="<?php bloginfo('template_directory'); ?>/assets/img/conduce.png" onmouseover="this.src='<?php bloginfo('template_directory'); ?>/assets/img/conduce-hover.png'" onmouseout="this.src='<?php bloginfo('template_directory'); ?>/assets/img/conduce.png'" alt="HOP" style="width:80%; margin:0 auto;">
          <div class="bk-sec-06__txt"><span>Conduce</span></div>
        </div>
      </div>
      <div class="col-sm-3">
        <div class="bk-sec-06__img">
          <img src="<?php bloginfo('template_directory'); ?>/assets/img/active.png" onmouseover="this.src='<?php bloginfo('template_directory'); ?>/assets/img/active-hover.png'" onmouseout="this.src='<?php bloginfo('template_directory'); ?>/assets/img/active.png'" alt="HOP" style="width:80%; margin:0 auto;">
          <div class="bk-sec-06__txt"><span>Actívate</span></div>
        </div>
      </div>
<!--       <div class="col-md-9">
        <div class="bk-sec-06__img">
          <picture class="d-block w-100 text-center">
            <source media="(min-width: 768px)" srcset="<?php // bloginfo('template_directory'); ?>/assets/img/tic.png">
            <img src="<?php //bloginfo('template_directory'); ?>/assets/img/icon-nimate.gif" alt="HOP" style="width:80%; margin:0 auto;">
          </picture>
        </div>
      </div> -->
    </div>
  </section>

  <section class="pt-4 section bg-07 bk-sec-07">
    <div class="container">
      <div class="row">
        <div class="col-md-5">
          <img class="bk-sec-07__logo" src="<?php bloginfo('template_directory'); ?>/assets/img/span-img.png" alt="First slide">
          <h2 class="bk-title--primary">CITY</h2>
          <p class="pt-5 pb-2">
            <!-- <a href="#" class="pr-5">
              <img src="<?php bloginfo('template_directory'); ?>/assets/img/facebook.png" alt="">
            </a> -->
            <a href="https://www.instagram.com/hopscooterchile/" class="pr-5">
              <img src="<?php bloginfo('template_directory'); ?>/assets/img/instagram.png" alt="">
            </a>
            <!-- <a href="#" class="pr-5">
              <img src="<?php bloginfo('template_directory'); ?>/assets/img/twitter.png" alt="">
            </a>
            <a href="#" class="pr-5">
              <img src="<?php bloginfo('template_directory'); ?>/assets/img/youtube.png" alt="">
            </a> -->
          </p>
<!--           <div class="">
            <a href="#" ><img class="pt-2" src="<?php bloginfo('template_directory'); ?>/assets/img/app-store.png" alt=""></a>
            <a href="#" ><img class="pt-2" src="<?php bloginfo('template_directory'); ?>/assets/img/play-store.png" alt=""></a>
          </div> -->
        </div>
      </div>
      <div class="bk-footer">
        <div class="container">
          <div class="row justify-content-between align-items-end">

            <div class="col-md-5 text-white">
              <h4>#DaelSalto y ten tu propia flota</h4>
              <p><small>© 2018 HOP Scooter. Todos los derechos reservados</small></p>
            </div>

            <div class="col-md-6">
              <ul class="d-flex bk-footer--menu text-center">
                <!-- <li>
                  <a href="#" target="_blank" class="p-2 bk-link-white">Soporte</a>
                </li>
                <li>
                  <a href="#" target="_blank" class="p-2 bk-link-white">Términos de uso</a>
                </li>
                <li>
                  <a href="#" target="_blank" class="p-2 bk-link-white">Politica de privacidad</a>
                </li> -->
              </ul>
            </div><!--Col-->

          </div><!--row-->
        </div><!--Extra Container-->

      </div><!--footer-->

    </div><!--Container-->

  </section>
  
</main>


<?php get_template_part('includes/footer'); ?>
