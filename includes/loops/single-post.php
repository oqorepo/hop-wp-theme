<?php
/*
The Single Post
===============
*/
?>

<?php
    $today = date("Ymd");
    $event_date =  get_field('fecha_del_evento');
    //echo $event_date." - ".$today;
?>
<section class="bk-section">
<?php if(have_posts()): while(have_posts()): the_post(); ?>
    <article role="article" id="post_<?php the_ID()?>" <?php post_class()?>>
        <header class="mb-4">
            <h2 class="text-center bk-title--red">
                <?php the_title()?>
            </h2>
            <?php if ( !is_singular( 'dojo' ) ) :?>
                <p class="text-center">
                    <?php _e('Categoría: ', 'b4st'); the_category(', '); ?>
                </p>
            <?php endif; ?>
        </header>
        <div class="container ">
            <div class="row bk-section">
                <div class="col">
                    <?php
                    the_content();
                    wp_link_pages();
                    ?>
                </div>
            </div>
        <?php if ($event_date):?>
            <?php //=================================// SI EL EVENTO ES FUTURO //==================================//
            if ($today <= $event_date):?>

            <div class="row mt-5 pt-4">
                <div class="col-sm">
                    <?php 
                    //=================================// CAMPO CRONOGRAMA //==================================//
                    if( have_rows('cronograma') ): ?>

                    <h3 class="bk-title--red">Programación</h3>
                    <hr>
                    <ul class="">
                        <p>Cronograma</p>

                    <?php while( have_rows('cronograma') ): the_row(); 
                        $hora = get_sub_field('hora_proramada');
                        $actividad = get_sub_field('nombre_de_actividad');
                    ?>

                        <li class="pt-2 pb-2">
                            <span class="bk-text--red"><?php echo $hora; ?></span>
                            <span> - </span>
                            <span><?php echo $actividad; ?></span>
                        </li>

                    <?php endwhile; ?>
                    </ul>
                    <?php endif; ?>
                </div>

                <div class="col-sm">
                    <?php //================================= CAMPO RUTAS //==================================
                    //$location = get_field('lugar_del_evento');

                    if( !empty($location) ): ?>
                        <h3 class="bk-title--red">Rutas</h3>
                        <hr>
                        <p>Lugar del Evento</p>
                        <div class="acf-map">
                            <div class="marker" data-lat="<?php echo $location['lat']; ?>" data-lng="<?php echo $location['lng']; ?>"></div>
                        </div>
                    <?php endif; ?>
                    <h3 class="bk-title--red">Rutas</h3>
                        <hr>
                        <p>Lugar del Evento</p>
                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3331.258578701079!2d-70.5531166842543!3d-33.39041750182089!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x9662ceca73eaece3%3A0x8a6fe7c6e9141117!2sComercial+Mx+s.a.!5e0!3m2!1ses!2scl!4v1535745428009" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
                </div>
            </div>

            <div class="row mt-5 pt-4">
                <div class="col-sm">
                    <?php //================================= CAMPO EQUIPAMIENTO //==================================
                    $equipamientos = get_field('equipamiento');

                    if( $equipamientos ): ?>
                    <h3 class="bk-title--red">Equipamiento</h3>
                    <hr>
                    <ul class="list-inline">
                        <p>Todo lo necesario para el evento</p>
                        <?php foreach( $equipamientos as $equipo ): ?>
                        <li class="list-inline-item"><span class="bk-<?php echo strtolower($equipo); ?>"></span><?php echo $equipo; ?></li>
                        <?php endforeach; ?>
                    </ul>
                    <?php endif; ?>
                </div>
                
                <div class="col-sm">
                    <?php //================================= CAMPO CONDICIÓN //==================================
                    $condiciones = get_field('condiciones');

                    if( $condiciones ): ?>
                    <h3 class="bk-title--red">Condiciones</h3>
                    <hr>
                    <ul class="list-inline">
                        <p>Clima e ingreso al evento</p>
                        <?php foreach( $condiciones as $condicion): ?>
                            <li class="list-inline-item text-center"><span class="bk-<?php echo strtolower($condicion); ?>"></span><?php echo $condicion; ?></li>
                        <?php endforeach; ?>
                    </ul>
                    <?php endif; ?>
                </div>
            </div>
            <div class="row mt-5 pt-4">
                <div class="col text-center">
                    <!-- <button type="button" class="bk--btn bk--btn__red" data-toggle="modal" data-target="#exampleModal">Inscríbete acá</button> -->
                        

                        <?php if ( have_rows('boton_compra') ): ?>
                        <?php while( have_rows('boton_compra') ): the_row(); 
                        $texto_btn = get_sub_field('texto_btn');
                        $product_id = get_sub_field('product_id');
                        $shortcode = sprintf(
                            '[wc_quick_buy_link product="%1$s" label=" %2$s " qty="1" type="button" htmlclass=" dib bk--btn bk--btn__black"]',
                            $product_id,
                            $texto_btn
                        );
                        ?>
                        <?php echo do_shortcode( $shortcode ); ?>
                        <?php endwhile; ?>
                        <?php endif; ?>
                </div>
            </div>
                <?php else: ?>
                <div class="row">
                    <?php  //================================= CAMPO GALERÍA //==================================
                    $images = get_field('galeria_pasado');
                    $size = 'full';
                    $medium = 'medium';

                    if( $images ): ?>
                            <?php foreach( $images as $image ): ?>
                            <div class="col-sm-4 mt-4">
                                <a href="<?php  echo $image['url']; ?>" data-toggle="lightbox" data-gallery="example-gallery">
                                    <img src="<?php echo $image['sizes']['large']; ?>" alt="<?php echo $image['alt']; ?>" class="img-fluid"/>
                                </a>
                            </div>
                            <?php endforeach; ?>
                    <?php endif; ?>
                </div>
            <?php endif; ?>
        <?php endif; ?>
        </div>
    </article>

<?php
  endwhile; else :
    get_template_part('./includes/loops/404');
  endif;
?>
<hr>
<div class="container">
    <div class="row pt-4">
        <div class="col">
            <?php previous_post_link('%link', '<i class="fas fa-fw fa-arrow-left"></i> Evento Anterior: '.'%title'); ?>
        </div>
        <div class="col text-right">
            <?php next_post_link('%link', 'Siguiente Evento: '.'%title' . ' <i class="fas fa-fw fa-arrow-right"></i>'); ?>
        </div>
    </div>
</div>

</section>
  <?php 
  $images1 = get_field('sponsors');
  $size1 = 'full'; // (thumbnail, medium, large, full or custom size)

  if( $images1 ): ?>
  <div class="container-fluid pt-5 pb-5 bk-sponsors">
      <div class="container">
          <h3 class="text-center bk-title--red">Sponsors</h3>
          <hr>
          <ul class="row bk-section">
              <?php foreach( $images1 as $image ): ?>
                  <li class="col text-center">
                    <?php echo wp_get_attachment_image( $image['ID'], $size ); ?>
                  </li>
              <?php endforeach; ?>
          </ul>
      </div>
  </div>
  <?php endif; ?>