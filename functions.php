<?php
/*
All the functions are in the PHP files in the `functions/` folder.
*/

require get_template_directory() . '/functions/cleanup.php';
require get_template_directory() . '/functions/setup.php';
require get_template_directory() . '/functions/enqueues.php';
require get_template_directory() . '/functions/develop.php';
require get_template_directory() . '/functions/custom-post.php';
require get_template_directory() . '/functions/disable-comments.php';
require get_template_directory() . '/functions/navbar.php';
require get_template_directory() . '/functions/widgets.php';
require get_template_directory() . '/functions/search-widget.php';
require get_template_directory() . '/functions/index-pagination.php';
require get_template_directory() . '/functions/single-split-pagination.php';
//require get_template_directory() . '/functions/maps.php';

add_action('admin_head', 'my_custom_fonts');

function my_custom_fonts() {
  echo '<style>
  #adminmenu .wp-menu-image img{
    max-height: 18px;
}
  </style>';
}

function dsgnwrks_qa_make_title_excerpted( $import ) {
	if ( isset( $import['post_title'] ) ) {
		// feel free to edit these 2 values
		$number_of_words = 4;
		$more = '...';
		$import['post_title'] = wp_trim_words( $import['post_title'], $number_of_words, $more );
	}
	return $import;
}
add_filter( 'dsgnwrks_instagram_pre_save', 'dsgnwrks_qa_make_title_excerpted' );

function dsgnwrks_instagram_save_postmeta_fields( $post_id, $instagram_photo_object, $post_args, $user_settings ) {
	$excerpted_caption = substr( $instagram_photo_object->caption->text, 0, 100 );
	update_post_meta( $post_id, 'excerpted_caption', $excerpted_caption );
}
add_action( 'dsgnwrks_instagram_post_save', 'dsgnwrks_instagram_save_postmeta_fields', 4 );