$(document).ready(function () {

    /* 
	------------------------------------------------------------------
		PRELOAD
	------------------------------------------------------------------
	*/

    $(window).on('load', function () {
        $('.cd-loader').fadeOut('slow', function () {
            $(this).remove();
        });
    });
    /* 
	------------------------------------------------------------------
		Menú Hamburguer
	------------------------------------------------------------------
	*/
    $('.hamburger').on('click', function () {
        $(this).toggleClass('is-active');
        $('.bk-primary-nav').toggleClass('loaded');
    });

    $('.bk-primary-nav__menu-item').on('click', function () {
        $('.bk-primary-nav').toggleClass('loaded');
        $('.hamburger').toggleClass('is-active');
    });
    /* 
	------------------------------------------------------------------
		Hover RIDE SECTION
	------------------------------------------------------------------
    */
    var mq = window.matchMedia('(min-width: 768px)');

    function WidthChange(mq) {
        if (mq.matches) {
            $(".bk-sec-06__img").hover(function () {
                $(this).find('.bk-sec-06__txt span').toggle('fast');
            });
        }
    }
    if (matchMedia) {
        var mq = window.matchMedia('(min-width: 768px)');
        mq.addListener(WidthChange);
        WidthChange(mq);
    }

    $('.grid .grid-item:nth-child(2)').addClass('grid-item--width2');
    $('.grid .grid-item:nth-child(3)').addClass('grid-item--height2');
    $('.grid .grid-item:nth-child(4)').addClass('grid-item--height2');
    $('.grid .grid-item:nth-child(6)').addClass('grid-item--width3');

});