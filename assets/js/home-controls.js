$(document).ready(function () {
    /* 
	------------------------------------------------------------------
		Fullpage Setup
	------------------------------------------------------------------
	*/
    $('#fullpage').fullpage({

        licenseKey: 'OPEN-SOURCE-GPLV3-LICENSE',
        //Navigation
        menu: '#menu',
        lockAnchors: false,
        anchors: [
            'daelSalto',
            'timeFun',
            'detail',
            'howTo',
            'hoppers',
            'sayHello',
            'social',
            'ride',
            'city'
        ],
        navigation: true,
        navigationPosition: 'right',
        navigationTooltips: [
            'DA EL SALTO',
            'HOP SCOOTER',
            'HOW TO HOP',
            'APP',
            'HOPPERS',
            'SAY HELLO!',
            'INSTA HOPPERS',
            'RIDE SAFE',
            'HOP CITY'
        ],
        showActiveTooltip: false,
        slidesNavigation: true,
        slidesNavPosition: 'bottom',

        //Scrolling
        responsiveWidth: 768,
        //css3: true,
        scrollingSpeed: 700,
        autoScrolling: true,
        fitToSection: true,
        fitToSectionDelay: 1000,
        scrollBar: false,
        easing: 'easeInOutCubic',
        //easingcss3: 'ease',
        fadingEffect: true,

        //Design
        controlArrows: true,
        responsiveSlides: true,
        parallax: true,
        parallaxOptions: {
            type: 'reveal',
            percentage: 62,
            property: 'translate'
        },

        //Custom selectors
        sectionSelector: '.section',
        slideSelector: '.bk-slide',

        lazyLoading: true
    });
    /* 
	------------------------------------------------------------------
		Lateral menu styles by section
	------------------------------------------------------------------
	*/
    $(window).on('hashchange', function () {

        var loc = window.location.href; // returns the full URL
        console.log(loc);

        if (loc.indexOf('#city') > -1 || loc.indexOf('#social') > -1) {
            $('#fp-nav ul li a span').css('background', 'white');
        }

        else if (loc.indexOf('#ride') > -1 || loc.indexOf('#saHello') > -1 ) {
            $('#fp-nav ul li a span').css('background', '#121212');
        }

        else if (loc.indexOf('#beHop') > -1 || loc.indexOf('#timeFun') > -1 || loc.indexOf('#detail') > -1 || loc.indexOf('#howTo') > -1 || loc.indexOf('#hoppers') > -1 ) {
            $('#fp-nav ul li a span').css('background', '#18FFFf');
        }

    });
    /* 
	------------------------------------------------------------------
		Image map setup
	------------------------------------------------------------------
	*/
    var xref = {
        TX: 'Frenos con tecnología E-ABS',
        BT: '25 km/h, 2km en 6 minutos',
        SD: 'Bateria HOP: Autonomía 55km',
        FR: 'Fibra de carbono inoxidable',
        MT: 'Capacidad de carga: 120kg'
    };

    $('.mapa').mapster({
        mapKey: 'data-key',
        listKey: 'data-key',
        singleSelect: true,
        showToolTip: true,
        toolTipClose: ["tooltip-click", "area-click"],
        onClick: function (e) {
            $('.bk-map--tooltip').removeClass('bk-map--tooltip__TX bk-map--tooltip__BT bk-map--tooltip__SD bk-map--tooltip__FR bk-map--tooltip__MT');
            $('.bk-map--tooltip').html('<p class="bk-tooltip">' + xref[e.key] + '</p>');
            $('.bk-map--tooltip').addClass('bk-map--tooltip__' + e.key);
        },
        areas: [{
            key: 'TX',
            altImage: 'http://oqodigital.net/hover-map5.png'
        },
        {
            key: 'BT',
            altImage: 'http://oqodigital.net/hover-map4.png'
        },
        {
            key: 'SD',
            altImage: 'http://oqodigital.net/hovermap-3.png'
        },
        {
            key: 'FR',
            altImage: 'http://oqodigital.net/hover-map2.png'
        },
        {
            key: 'MT',
            altImage: 'http://oqodigital.net/hover-map.png'
        }
        ]
    });

    /* 
	------------------------------------------------------------------
		MAPS Hover tooltips
	------------------------------------------------------------------
	*/
    $(".dk-TX").hover(function () {
        $('.bk-map--tooltip').removeClass('bk-map--tooltip__TX bk-map--tooltip__BT bk-map--tooltip__SD bk-map--tooltip__FR bk-map--tooltip__MT');
        $(".bk-map--tooltip").addClass('bk-map--tooltip__TX').html($(this).attr("alt")).toggle();
    });

    $(".dk-BT").hover(function () {
        $('.bk-map--tooltip').removeClass('bk-map--tooltip__TX bk-map--tooltip__BT bk-map--tooltip__SD bk-map--tooltip__FR bk-map--tooltip__MT');
        $(".bk-map--tooltip").addClass('bk-map--tooltip__BT').html($(this).attr("alt")).toggle();
    });

    $(".dk-SD").hover(function () {
        $('.bk-map--tooltip').removeClass('bk-map--tooltip__TX bk-map--tooltip__BT bk-map--tooltip__SD bk-map--tooltip__FR bk-map--tooltip__MT');
        $(".bk-map--tooltip").addClass('bk-map--tooltip__SD').html($(this).attr("alt")).toggle();
    });

    $(".dk-FR").hover(function () {
        $('.bk-map--tooltip').removeClass('bk-map--tooltip__TX bk-map--tooltip__BT bk-map--tooltip__SD bk-map--tooltip__FR bk-map--tooltip__MT');
        $(".bk-map--tooltip").addClass('bk-map--tooltip__FR').html($(this).attr("alt")).toggle();
    });
    $(".dk-MT").hover(function () {
        $('.bk-map--tooltip').removeClass('bk-map--tooltip__TX bk-map--tooltip__BT bk-map--tooltip__SD bk-map--tooltip__FR bk-map--tooltip__MT');
        $(".bk-map--tooltip").addClass('bk-map--tooltip__MT').html($(this).attr("alt")).toggle();
    });
    /* 
	------------------------------------------------------------------
		Text Animation
	------------------------------------------------------------------
	*/
    //set animation timing
    var animationDelay = 5400,
        //loading bar effect
        barAnimationDelay = 5400,
        barWaiting = barAnimationDelay - 5200;

    initHeadline();

    function initHeadline() {
        //insert <i> element for each letter of a changing word
        singleLetters($('.cd-headline.letters').find('.bk-hop-txt'));
        //initialise headline animation
        animateHeadline($('.cd-headline'));
    }

    function singleLetters($words) {
        $words.each(function () {
            var word = $(this),
                letters = word.text().split(''),
                selected = word.hasClass('is-visible');
            for (i in letters) {
                if (word.parents('.rotate-2').length > 0) letters[i] = '<em>' + letters[i] + '</em>';
                letters[i] = (selected) ? '<i class="in">' + letters[i] + '</i>' : '<i>' + letters[i] + '</i>';
            }
            var newLetters = letters.join('');
            word.html(newLetters).css('opacity', 1);
        });
    }

    function animateHeadline($headlines) {
        var duration = animationDelay;
        $headlines.each(function () {
            var headline = $(this);

            if (headline.hasClass('loading-bar')) {
                duration = barAnimationDelay;
                setTimeout(function () {
                    headline.find('.cd-words-wrapper').addClass('is-loading')
                }, barWaiting);
            };

            //trigger animation
            setTimeout(function () {
                hideWord(headline.find('.is-visible').eq(0))
            }, duration);
        });
    }

    function hideWord($word) {
        var nextWord = takeNext($word);

        if ($word.parents('.cd-headline').hasClass('loading-bar')) {
            $word.parents('.cd-words-wrapper').removeClass('is-loading');
            switchWord($word, nextWord);
            setTimeout(function () {
                hideWord(nextWord)
            }, barAnimationDelay);
            setTimeout(function () {
                $word.parents('.cd-words-wrapper').addClass('is-loading')
            }, barWaiting);

        } else {
            switchWord($word, nextWord);
            setTimeout(function () {
                hideWord(nextWord)
            }, animationDelay);
        }
    }

    function takeNext($word) {
        return (!$word.is(':last-child')) ? $word.next() : $word.parent().children().eq(0);
    }

    function takePrev($word) {
        return (!$word.is(':first-child')) ? $word.prev() : $word.parent().children().last();
    }

    function switchWord($oldWord, $newWord) {
        $oldWord.removeClass('is-visible').addClass('is-hidden');
        $newWord.removeClass('is-hidden').addClass('is-visible');
    }

    /* 
	------------------------------------------------------------------
		Slider Captions
	------------------------------------------------------------------
	*/
    $("#slider").on('slide.bs.carousel', function (evt) {

        var step = $(evt.relatedTarget).index();

        $('#slider_captions .bk-carousel-caption:not(#caption-' + step + ')').fadeOut('fast', function () {
        });
        $('#caption-' + step).fadeIn('slow');

    });

    /* 
	------------------------------------------------------------------
		Grid INIT Section 5
	------------------------------------------------------------------
	*/
    var $grid = $('.grid').imagesLoaded(function () {
        $grid.masonry({
            itemSelector: '.grid-item',
            percentPosition: true
        });
    });

    /* 
	------------------------------------------------------------------
		lightbox
	------------------------------------------------------------------
    */
    $(document).on('click', '[data-toggle="lightbox"]', function (event) {
        event.preventDefault();
        $(this).ekkoLightbox();
    });
});