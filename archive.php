<?php get_template_part('includes/header'); ?>
<img src="<?php bloginfo('template_directory'); ?>/assets/img/dojo.png" alt="">
<section class="container mt-5">
  <h2 class="text-center bk-title--red">Suzuki Dojo</h2>
  <div class="row mt-5 mb-5">
    <div class="col">
      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam blandit ligula eget dui tempus porta. Etiam vestibulum aliquet vestibulum. Pellentesque bibendum vulputate dolor eget dignissim. Mauris luctus, nulla eu mattis facilisis, ligula urna tincidunt sem, at laoreet mi mi eu neque. Mauris semper turpis at elit euismod hendrerit. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam blandit ligula eget dui tempus porta. Etiam vestibulum aliquet vestibulum. Pellentesque bibendum vulputate dolor eget dignissim.</p>
    </div>
  </div>
  <?php get_template_part('includes/loops/index-loop'); ?>
</section><!-- /.container -->

<?php get_template_part('includes/footer'); ?>
