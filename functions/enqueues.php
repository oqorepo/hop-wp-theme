<?php
/**!
 * Enqueues
 */
$url = 'https://code.jquery.com/jquery-latest.min.js';
$test_url = @fopen($url,'r');
if($test_url !== false) {
	function load_external_jQuery() {
		wp_deregister_script('jquery');
		wp_register_script('jquery', 'https://code.jquery.com/jquery-latest.min.js');
		wp_enqueue_script('jquery');
	}
	add_action('wp_enqueue_scripts', 'load_external_jQuery');
} else {
	function load_local_jQuery() {
		wp_deregister_script('jquery');
		wp_register_script('jquery', get_bloginfo('template_url').'./assets/js/jquery.min.js', __FILE__, false, '1.11.3', true);
		wp_enqueue_script('jquery');
	}
	add_action('wp_enqueue_scripts', 'load_local_jQuery');
}


if ( ! function_exists('b4st_enqueues') ) {
	function b4st_enqueues() {

		// Styles

		wp_register_style('bootstrap-css', 'https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.1/css/bootstrap.min.css', false, '4.1.1', null);
		wp_enqueue_style('bootstrap-css');

		// Scripts

		wp_register_script('font-awesome-config-js', get_template_directory_uri() . '/assets/js/font-awesome-config.js', false, null, null);
		wp_enqueue_script('font-awesome-config-js');

		wp_register_script('font-awesome', 'https://use.fontawesome.com/releases/v5.0.13/js/all.js', false, '5.0.13', null);
		wp_enqueue_script('font-awesome');

		wp_register_script('modernizr',  'https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js', false, '2.8.3', true);
		wp_enqueue_script('modernizr');

		wp_register_script('popper',  'https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js', false, '1.14.3', true);
		wp_enqueue_script('popper');

		wp_register_script('bootstrap-js', 'https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.1/js/bootstrap.min.js', false, '4.1.1', true);
		wp_enqueue_script('bootstrap-js');

		if (is_singular() && comments_open() && get_option('thread_comments')) {
			wp_enqueue_script('comment-reply');
		}

	}
}
add_action('wp_enqueue_scripts', 'b4st_enqueues', 100);

function boken_enqueues() {

	if (is_front_page()){

		wp_register_script('masonry-js', get_template_directory_uri() . '/assets/js/masonry.pkgd.min.js', false, null);
		wp_enqueue_script('masonry-js');

		wp_register_script('masonry-ext-js', 'https://unpkg.com/imagesloaded@4/imagesloaded.pkgd.min.js', false, null, true);
		wp_enqueue_script('masonry-ext-js');

		wp_register_style('fullpage-css', get_template_directory_uri() . '/assets/css/fullpage.min.css', false, null);
		wp_enqueue_style('fullpage-css');

		wp_register_script('fullpage-js', get_template_directory_uri() . '/assets/js/fullpage.min.js', false, null, true);
		wp_enqueue_script('fullpage-js');

		wp_register_script('fullpage-ext-js', 'https://cdnjs.cloudflare.com/ajax/libs/fullPage.js/3.0.2/fullpage.extensions.min.js', false, null, true);
		wp_enqueue_script('fullpage-ext-js');

		wp_register_script('img-map-js', get_template_directory_uri() . '/assets/js/jquery.imagemapster.min.js', false, null, true);
		wp_enqueue_script('img-map-js');

		wp_register_style( 'lightbox-css', 'https://cdnjs.cloudflare.com/ajax/libs/ekko-lightbox/5.3.0/ekko-lightbox.css' );
		wp_enqueue_style('lightbox-css');

		wp_register_script('lightbox-js', 'https://cdnjs.cloudflare.com/ajax/libs/ekko-lightbox/5.3.0/ekko-lightbox.js');
		wp_enqueue_script('lightbox-js');

		wp_register_script('home-controsl-js', get_template_directory_uri() . '/assets/js/home-controls.js', false, null, true);
		wp_enqueue_script('home-controsl-js');
	}
	wp_register_style('boken-css', get_template_directory_uri() . '/assets/css/main.css', false, null);
	wp_enqueue_style('boken-css');  

	wp_register_script('boken-js', get_template_directory_uri() . '/assets/js/main.js', false, null, true);
	wp_enqueue_script('boken-js'); 

}
add_action('wp_enqueue_scripts', 'boken_enqueues', 100);